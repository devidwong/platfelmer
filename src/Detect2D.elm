module Detect2D exposing (detect2D)

import Constants exposing (dt)


detect2D mario vx vy obstacles =
    case obstacles of
        [] ->
            []

        obstacle :: tl ->
            detect mario vx vy obstacle
                :: detect2D mario vx vy tl


detect mario vx vy obstacle =
    let
        newX =
            mario.x + dt * vx

        newY =
            mario.y + dt * vy

        top =
            topEdge obstacle

        right =
            rightEdge obstacle mario.w

        bottom =
            bottomEdge obstacle mario.h

        left =
            leftEdge obstacle mario.w

        isAboveOrUnder =
            left <= mario.x && mario.x <= right

        isOnTheLeftOrRight =
            bottom <= mario.y && mario.y <= top

        crossingTop =
            top <= mario.y && newY <= top

        crossingBottom =
            mario.y <= bottom && bottom <= newY

        crossingLeft =
            mario.x <= left && left <= newX

        crossingRight =
            right <= mario.x && newX <= right
    in
    if isAboveOrUnder && crossingTop then
        "floor"

    else if isAboveOrUnder && crossingBottom then
        "ceil"

    else if isOnTheLeftOrRight && crossingLeft then
        "wallOnRight"

    else if isOnTheLeftOrRight && crossingRight then
        "wallOnLeft"

    else
        "move"


leftEdge obstacle w =
    obstacle.x - obstacle.w / 2 - w / 2


rightEdge obstacle w =
    obstacle.x + obstacle.w / 2 + w / 2


topEdge obstacle =
    obstacle.y + obstacle.h


bottomEdge obstacle h =
    obstacle.y - h
