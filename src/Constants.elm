module Constants exposing (..)

import Level exposing (enemies)


initial =
    { x = 0
    , y = 0
    , w = 32
    , h = 54
    , vx = 0
    , vy = 0
    , dir = "right"
    , obstacles = []
    , enemies = enemies
    }


dt =
    2


maxVY =
    5


weightlessness =
    10
