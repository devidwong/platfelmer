module Enemies exposing (..)

import List exposing (..)


enemyA =
    { w = 50
    , h = 50
    , x = 500
    , y = 0
    , dir = "left"
    }


enemyB =
    { w = 50
    , h = 50
    , x = 700
    , y = 0
    , dir = "left"
    }


enemyC =
    { w = 50
    , h = 50
    , x = 900
    , y = 0
    , dir = "left"
    }


enemyD =
    { w = 50
    , h = 50
    , x = -900
    , y = 0
    , dir = "right"
    }


enemyE =
    { w = 50
    , h = 50
    , x = -300
    , y = 0
    , dir = "left"
    }


enemiesMove enemies =
    case enemies of
        [] ->
            []

        enemy :: rest ->
            if enemy.dir == "left" then
                { w = enemy.w
                , h = enemy.h
                , x = enemy.x - 1
                , y = enemy.y
                , dir = enemy.dir
                }
                    :: enemiesMove rest

            else
                { w = enemy.w
                , h = enemy.h
                , x = enemy.x + 1
                , y = enemy.y
                , dir = enemy.dir
                }
                    :: enemiesMove rest


enemiesSmash bites enemies =
    case enemies of
        [] ->
            []

        enemy :: restEnemies ->
            case bites of
                [] ->
                    []

                bite :: restBites ->
                    if "floor" == bite then
                        enemiesSmash restBites restEnemies

                    else
                        enemy :: enemiesSmash restBites restEnemies


enemiesHitWalls detect obstacles enemies =
    case enemies of
        [] ->
            []

        enemy :: restEnemies ->
            let
                newDir =
                    if enemy.dir == "right" then
                        "left"

                    else
                        "right"
            in
            if enemyHitsWall detect enemy obstacles then
                { w = enemy.w
                , h = enemy.h
                , x = enemy.x
                , y = enemy.y
                , dir = newDir
                }
                    :: enemiesHitWalls detect obstacles restEnemies

            else
                enemy :: enemiesHitWalls detect obstacles restEnemies


enemyHitsWall detect enemy obstacles =
    if enemy.dir == "left" then
        member "wallOnLeft" (detect enemy -1 0 obstacles)

    else
        member "wallOnRight" (detect enemy 1 0 obstacles)


enemiesBurn detect lavas enemies =
    case enemies of
        [] ->
            []

        enemy :: restEnemies ->
            if enemyBurn detect enemy lavas then
                enemiesBurn detect lavas restEnemies

            else
                enemy :: enemiesBurn detect lavas restEnemies


enemyBurn detect enemy lavas =
    if enemy.dir == "left" then
        member "wallOnLeft" (detect enemy -1 0 lavas)

    else
        member "wallOnRight" (detect enemy 1 0 lavas)
