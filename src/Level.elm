module Level exposing (..)

import Enemies exposing (..)
import Lava exposing (..)
import Obstacles exposing (..)


obstacles =
    [ obstacleA
    , obstacleB
    , obstacleC
    , obstacleD
    ]


lava =
    [ lavaA
    ]


enemies =
    [ enemyA
    , enemyB
    , enemyC
    , enemyD
    , enemyE
    ]
