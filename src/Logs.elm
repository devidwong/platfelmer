module Logs exposing (logs)

import Playground exposing (..)


logs mario =
    toTopLeft
        [ logFloat mario.x "x"
        , logFloat mario.y "y"
        , logFloat mario.vy "vy"
        , logFloat mario.vx "vx"
        , log mario.dir "dir"
        ]


logFloat var =
    var |> floor |> String.fromInt |> log


logBool var =
    if var then
        log "True"

    else
        log "False"


log var desc =
    words black (desc ++ ": " ++ var)


toTopLeft =
    toTopLeft_ 1


toTopLeft_ index shapes scr =
    let
        marginV =
            40

        marginX =
            2 * marginV

        spaceBetween =
            marginV * index

        x =
            scr.left + marginX

        y =
            scr.top - marginV - spaceBetween
    in
    case shapes of
        [] ->
            []

        hd :: tl ->
            move x y hd :: toTopLeft_ (index + 1) tl scr
