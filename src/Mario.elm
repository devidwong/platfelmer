module Mario exposing (..)

import Constants exposing (..)
import Detect2D exposing (..)
import Enemies exposing (..)
import Level exposing (..)
import List exposing (..)
import Logs exposing (logs)
import Playground exposing (..)



-- MAIN


main =
    game view update initial



-- VIEW


view computer mario =
    let
        s =
            computer.screen
    in
    concat
        [ background s
        , obstacles
            |> includeUserInput mario
            |> position black
        , lava
            |> includeUserInput mario
            |> position red
        , mario.enemies
            |> includeUserInput mario
            |> positionImage "images/mush.png"
        , char mario

        --, logs mario s
        ]



-- UPDATE


update computer mario =
    let
        keys =
            computer.keyboard

        vx =
            toX keys

        gravityVY =
            mario.vy - dt / weightlessness

        detectCharCollisions =
            detect2D mario vx gravityVY

        obstacleCharCollisions =
            detectCharCollisions obstacles

        lavaCharCollisions =
            detectCharCollisions lava

        enemyCharCollisions =
            detectCharCollisions mario.enemies

        enemies =
            mario.enemies
                |> enemiesMove
                |> enemiesHitWalls detect2D obstacles
                |> enemiesBurn detect2D lava
                |> enemiesSmash enemyCharCollisions

        gameOver =
            any wound lavaCharCollisions || any bite enemyCharCollisions

        feetHit =
            member "floor" obstacleCharCollisions

        headHit =
            member "ceil" obstacleCharCollisions

        leftHit =
            member "wallOnRight" obstacleCharCollisions

        rightHit =
            member "wallOnLeft" obstacleCharCollisions

        vy =
            if feetHit then
                if keys.up then
                    maxVY

                else
                    0

            else if headHit then
                0

            else
                gravityVY

        x =
            if leftHit || rightHit then
                mario.x

            else
                mario.x + dt * vx

        y =
            if feetHit then
                mario.y

            else
                mario.y + dt * vy
    in
    if gameOver then
        initial

    else
        { x = x
        , y = y
        , w = mario.w
        , h = mario.h
        , vx = vx
        , vy = vy
        , dir = direction mario
        , obstacles = obstacleCharCollisions
        , enemies = enemies
        }



-- HELPERS


direction mario =
    if mario.vx == 0 then
        mario.dir

    else if mario.vx < 0 then
        "left"

    else
        "right"


background screen =
    let
        blue =
            rgb 174 238 238
    in
    [ rectangle blue screen.width screen.height
    ]


char mario =
    let
        actor =
            toGif mario

        y =
            ground (mario.h + 20) (mario.y - 10)
    in
    [ image (mario.w + 40) (mario.h + 20) actor
        |> moveY y
    ]


includeUserInput mario level =
    case level of
        [] ->
            []

        shape :: rest ->
            { shape | x = shape.x - mario.x }
                :: includeUserInput mario rest


position color level =
    case level of
        [] ->
            []

        shape :: rest ->
            let
                actor =
                    rectangle color shape.w shape.h

                y =
                    ground shape.h shape.y
            in
            move shape.x y actor
                :: position color rest


positionImage path level =
    case level of
        [] ->
            []

        shape :: rest ->
            let
                y =
                    ground shape.h shape.y
            in
            move shape.x y (image shape.h shape.h path)
                :: positionImage path rest


toGif mario =
    if member "floor" mario.obstacles then
        "images/mario/stand/" ++ mario.dir ++ ".gif"

    else
        "images/mario/jump/" ++ mario.dir ++ ".gif"


ground h y =
    (h / 2) + y


wound bodyPart =
    bodyPart /= "move"


bite bodyPart =
    bodyPart /= "move" && bodyPart /= "floor"
