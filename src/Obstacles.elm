module Obstacles exposing (..)


obstacleA =
    { w = 50
    , h = 50
    , x = -100
    , y = 0
    }


obstacleB =
    { w = 50
    , h = 50
    , x = 44
    , y = 56
    }


obstacleC =
    { w = 50
    , h = 50
    , x = -70
    , y = 106
    }


obstacleD =
    { w = 10000
    , h = 50
    , x = 0
    , y = -50
    }
