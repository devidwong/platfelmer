# Platfelmer
## 2D platform game

It was insplired by SVG based game engine for Elm, a web development purpose, functional language
https://package.elm-lang.org/packages/evancz/elm-playground/latest/
https://elm-lang.org/

To run it reactively in a browser:

* Install elm https://guide.elm-lang.org/install/elm.html
* run `elm reactor` command in the root of this repo

To compile it down to JS:

* run `elm make`
* serve produced `index.html` file in a hosting or with any kind of static HTTP server
